import com.fasterxml.jackson.databind.ObjectMapper
import edu.eam.ingesoft.academic.repositories.AcademicProgramRepository
import edu.eam.ingesoft.academic.repositories.AcademicSpaceRepository
import edu.eam.ingesoft.academic.repositories.ContractLoadRepository
import edu.eam.ingesoft.academic.repositories.FacultyRepository
import edu.eam.ingesoft.academic.security.authclient.SecurityClient
import edu.eam.ingesoft.academic.security.authclient.model.Header
import edu.eam.ingesoft.academic.security.authclient.model.SecurityPayload
import edu.eam.ingesoft.academic.security.authclient.model.Token
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.springframework.transaction.annotation.Transactional

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
@Transactional
abstract class CommonTests {

    @Autowired
    protected lateinit var objectMapper: ObjectMapper

    @Autowired
    protected lateinit var academicSpaceRepository: AcademicSpaceRepository

    @Autowired
    protected lateinit var facultyRepository: FacultyRepository

    @Autowired
    protected lateinit var academicProgramRepository: AcademicProgramRepository

    @Autowired
    protected lateinit var mockMvc: MockMvc

    @Autowired
    protected lateinit var contractLoadRepository: ContractLoadRepository

    @MockBean
    protected lateinit var securityClient: SecurityClient

    protected fun mockSecurity(
        tokenValue: String = "SecurityToken",
        permissions: List<String> = listOf(),
        groups: List<String> = listOf()
    ): Header {
        val token = Token(tokenValue)
        val payload = SecurityPayload("username", permissions, groups)
        Mockito.`when`(securityClient.validateToken(token)).thenReturn(payload)

        return Header("Authorization", "Bearer $tokenValue")
    }
}
