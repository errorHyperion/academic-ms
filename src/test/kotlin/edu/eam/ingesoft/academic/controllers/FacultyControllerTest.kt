package edu.eam.ingesoft.academic.controllers

import CommonTests
import edu.eam.ingesoft.academic.config.Groups
import edu.eam.ingesoft.academic.config.Permissions
import edu.eam.ingesoft.academic.config.Routes
import org.hamcrest.Matchers
import org.junit.Assert
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

class FacultyControllerTest : CommonTests() {
    @Test
    fun createFacultyTest() {
        val id: Long = 1L
        val name: String = "faculty1"
        val status: Boolean = true

        val jsonBody =
            """{
        "id" : "$id",
        "name": "$name",
        "status" : "$status"
        }"""

        val header = mockSecurity("Token", listOf(Permissions.CREATE_FACULTY), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders
            .post(Routes.FACULTY_PATH)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(header.name, header.value)
        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)
    }

    @Test
    @Sql("/queries/createFacultyAlreadyExistTest.sql")
    fun createFacultyAlreadyExist() {

        val id: Long = 1L
        val name: String = "faculty1"
        val status: Boolean = true

        val jsonBody =
            """{
        "id" : "$id",
        "name": "$name",
        "status" : "$status"
        }"""

        val header = mockSecurity("Token", listOf(Permissions.CREATE_FACULTY), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders
            .post(Routes.FACULTY_PATH)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)
        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("The faculty is already created")))
    }

    @Test
    @Sql("/queries/findFacultyTest.sql")
    fun findFacultyTest() {
        val id: Long = 1L

        val header = mockSecurity("Token", listOf(Permissions.FIND_FACULTY), listOf(Groups.SYSTEM_ADMINISTRATOR, Groups.PROGRAM_DIRECTOR))
        val request = MockMvcRequestBuilders
            .get("${Routes.FACULTY_PATH}${Routes.SEARCH_FACULTY_PATH}", id)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)
        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)
        val assignmentList = facultyRepository.findAll().asSequence().toList()
        val facultyToAssert = assignmentList.find { it.id == 1L }
        Assert.assertNotNull(facultyToAssert)
        Assert.assertEquals(id, facultyToAssert?.id)
    }

    @Test
    fun findFacultyAlreadyExist() {
        val header = mockSecurity("Token", listOf(Permissions.CREATE_FACULTY), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders
            .get("${Routes.FACULTY_PATH}/3")
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)
        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("faculty Not Found")))
    }

    @Test
    @Sql("/queries/editFacultyTest.sql")
    fun editFacultyTest() {
        val id: Long = 1L
        val name: String = "faculty1"
        val status: Boolean = true

        val jsonBody =
            """{
        "name": "$name",
        "status" : "$status"
        }"""

        val header = mockSecurity("Token", listOf(Permissions.EDIT_FACULTY), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders
            .put("${Routes.FACULTY_PATH}/1")
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)
        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)
        val assignmentList = facultyRepository.findAll().asSequence().toList()
        val facultyToAssert = assignmentList.find { it.id == 1L }
        Assert.assertNotNull(facultyToAssert)
        Assert.assertEquals(id, facultyToAssert?.id)
    }

    @Test
    fun editFacultyAlreadyExistTest() {

        val name: String = "faculty1"
        val status: Boolean = true

        val jsonBody =
            """{
        "name": "$name",
        "status" : "$status"
        }"""

        val header = mockSecurity("Token", listOf(Permissions.EDIT_FACULTY), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders
            .put("${Routes.FACULTY_PATH}/1")
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)
        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Faculty Not Found")))
    }

    @Test
    @Sql("/queries/ActivateFacultyTest.sql")
    fun ActivateFacultyTest() {

        val id: Long = 1L

        val header = mockSecurity("Token", listOf(Permissions.ACTIVATE_FACULTY), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders
            .patch("${Routes.FACULTY_PATH}${Routes.ACTIVATE_FACULTY_PATH}", id)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)
        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)
        val assignmentList = facultyRepository.findAll().asSequence().toList()
        val facultyToAssert = assignmentList.find { it.id == 1L }
        Assert.assertNotNull(facultyToAssert)
        Assert.assertEquals(1L, facultyToAssert?.id)
    }

    @Test
    fun ActivateFacultyNotFoundTest() {

        val id: Long = 1L

        val header = mockSecurity("Token", listOf(Permissions.ACTIVATE_FACULTY), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders
            .patch("${Routes.FACULTY_PATH}${Routes.ACTIVATE_FACULTY_PATH}", id)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)
        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Faculty doesn't exist.")))
    }

    @Test
    @Sql("/queries/ActivateFacultyAlreadyActivateTest.sql")
    fun ActivateFacultyAlreadyActivateTest() {

        val id: Long = 1L

        val header = mockSecurity("Token", listOf(Permissions.ACTIVATE_FACULTY), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders
            .patch("${Routes.FACULTY_PATH}${Routes.ACTIVATE_FACULTY_PATH}", id)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)
        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Faculty already activated.")))
    }

    @Test
    @Sql("/queries/disableFacultyTest.sql")
    fun disableFacultyTest() {

        val id: Long = 1L

        val header = mockSecurity("Token", listOf(Permissions.DEACTIVATE_FACULTY), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders
            .patch("${Routes.FACULTY_PATH}${Routes.DISABLE_FACULTY_PATH}", id)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)
        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)
        val assignmentList = facultyRepository.findAll().asSequence().toList()
        val facultyToAssert = assignmentList.find { it.id == 1L }
        Assert.assertNotNull(facultyToAssert)
        Assert.assertEquals(false, facultyToAssert?.status)
    }

    @Test
    fun disableNullFacultyTest() {

        val id: Long = 1L

        val header = mockSecurity("Token", listOf(Permissions.DEACTIVATE_FACULTY), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders
            .patch("${Routes.FACULTY_PATH}${Routes.DISABLE_FACULTY_PATH}", id)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)
        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Faculty doesn't exist.")))
    }

    @Test
    @Sql("/queries/disableFalseFacultyTest.sql")
    fun disableFalseFacultyTest() {

        val id: Long = 1L

        val header = mockSecurity("Token", listOf(Permissions.DEACTIVATE_FACULTY), listOf(Groups.SYSTEM_ADMINISTRATOR))
        val request = MockMvcRequestBuilders
            .patch("${Routes.FACULTY_PATH}${Routes.DISABLE_FACULTY_PATH}", id)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)
        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Faculty already deactivate.")))
    }
}
