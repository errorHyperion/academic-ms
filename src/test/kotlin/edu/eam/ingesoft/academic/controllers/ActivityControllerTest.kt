package edu.eam.ingesoft.academic.controllers

import CommonTests
import edu.eam.ingesoft.academic.config.Routes
import org.hamcrest.Matchers
import org.junit.jupiter.api.Test
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

class ActivityControllerTest : CommonTests() {

    @Test
    @Sql("/queries/ActivityTest/findActivityTest.sql")
    fun findActivityTest() {
        val activityId: Long = 1L

        val request = MockMvcRequestBuilders
            .get("${Routes.ACTIVITY_PATH}${Routes.FIND_ACTIVITY_PATH}", activityId)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.`is`(1)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.`is`("Docencia directa")))
            .andExpect(MockMvcResultMatchers.jsonPath("$.description", Matchers.`is`("Docencia directa")))
    }

    @Test
    fun findActivityThatNotExistTest() {
        val activityId: Long = 1L

        val request = MockMvcRequestBuilders
            .get("${Routes.ACTIVITY_PATH}${Routes.FIND_ACTIVITY_PATH}", activityId)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Activity Not Found")))
    }
}
