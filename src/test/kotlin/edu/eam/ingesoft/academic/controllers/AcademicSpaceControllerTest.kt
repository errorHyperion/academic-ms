package edu.eam.ingesoft.academic.controllers

import CommonTests
import edu.eam.ingesoft.academic.config.Groups
import edu.eam.ingesoft.academic.config.Permissions
import edu.eam.ingesoft.academic.config.Routes
import edu.eam.ingesoft.academic.model.entities.AcademicProgram
import edu.eam.ingesoft.academic.model.entities.AcademicSpace
import edu.eam.ingesoft.academic.model.entities.Faculty
import org.hamcrest.Matchers
import org.junit.Assert
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

class AcademicSpaceControllerTest : CommonTests() {
    @Test
    @Sql("/queries/AcademicSpaceControllerTest/createAcademicSpace.sql")
    fun createAcademicSpaceTest() {
        val academicProgram = academicProgramRepository.findById(1).orElse(null)
        val academicSpace = AcademicSpace(3, "academicspace2", 8, 10, "modality1", academicProgram, true, 3)

        val header = mockSecurity("Token", listOf(Permissions.CREATE_ACADEMIC_SPACE), listOf(Groups.SYSTEM_ADMINISTRATOR, Groups.PROGRAM_DIRECTOR))
        val request = MockMvcRequestBuilders.post(Routes.ACADEMIC_SPACES_PATH)
            .content(objectMapper.writeValueAsString(academicSpace)).contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)
        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)

        val academicSpaces = academicSpaceRepository.findAll().asSequence().toList()

        val academicSpaceToAssert = academicSpaces.find { it.name == academicSpace.name }

        Assert.assertNotNull(academicSpaceToAssert)
        Assert.assertEquals(academicProgram.name, academicProgram.name)
    }

    @Test
    @Sql("/queries/AcademicSpaceControllerTest/createAcademicSpaceNameInUse.sql")
    fun createAcademicSpaceNameInUseTest() {
        val academicProgram = academicProgramRepository.findById(1).orElse(null)
        val academicSpace = AcademicSpace(3, "name1", 8, 10, "modality2", academicProgram, true, 3)

        val header = mockSecurity("Token", listOf(Permissions.CREATE_ACADEMIC_SPACE), listOf(Groups.SYSTEM_ADMINISTRATOR, Groups.PROGRAM_DIRECTOR))
        val request = MockMvcRequestBuilders.post(Routes.ACADEMIC_SPACES_PATH)
            .content(objectMapper.writeValueAsString(academicSpace))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(
                MockMvcResultMatchers.jsonPath(
                    "$.message",
                    Matchers.`is`("Name in use")
                )
            )
    }

    @Test
    fun createAcademicSpaceProgramDoesntExistTest() {
        val faculty = Faculty(2, "Ingenieria", true)
        val academicProgram = AcademicProgram(1, "nameee", "sniees", 10, faculty, 5, true)
        val academicSpace = AcademicSpace(3, "name1", 8, 10, "modality2", academicProgram, true, 4)

        val header = mockSecurity("Token", listOf(Permissions.CREATE_ACADEMIC_SPACE), listOf(Groups.SYSTEM_ADMINISTRATOR, Groups.PROGRAM_DIRECTOR))
        val request = MockMvcRequestBuilders.post(Routes.ACADEMIC_SPACES_PATH)
            .content(objectMapper.writeValueAsString(academicSpace))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("The Program does not exist!")))
    }

    @Test
    @Sql("/queries/AcademicSpaceControllerTest/findAcademicSpace.sql")
    fun findAcademicSpaceTest() {

        val idAcademicSpace: Long = 3L

        val header = mockSecurity("Token", listOf(Permissions.FIND_ACADEMIC_SPACE), listOf(Groups.SYSTEM_ADMINISTRATOR, Groups.PROGRAM_DIRECTOR))
        val request = MockMvcRequestBuilders.get(
            "${Routes.ACADEMIC_SPACES_PATH}/${Routes.FIND_ACADEMIC_SPACES_PATH}",
            idAcademicSpace
        )

            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(header.name, header.value)
        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.`is`(3)))
    }

    @Test
    @Sql("/queries/AcademicSpaceControllerTest/findAcademicSpaceNotFound.sql")
    fun findAcademicSpaceNotFoundTest() {
        val idAcademicSpace: Long = 5L

        val header = mockSecurity("Token", listOf(Permissions.FIND_ACADEMIC_SPACE), listOf(Groups.SYSTEM_ADMINISTRATOR, Groups.PROGRAM_DIRECTOR))
        val request = MockMvcRequestBuilders.get(
            "${Routes.ACADEMIC_SPACES_PATH}/${Routes.FIND_ACADEMIC_SPACES_PATH}",
            idAcademicSpace
        )
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(header.name, header.value)
        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Academic Space not found.")))
    }

    @Test
    @Sql("/queries/AcademicSpaceControllerTest/editAcademicSpaces.sql")
    fun editAcademicSpacesTest() {
        val academicProgram = academicProgramRepository.findById(1).orElse(null)
        val academicSpace = AcademicSpace(3, "name5", 8, 10, "modality2", academicProgram, true, 8)

        val header = mockSecurity("Token", listOf(Permissions.EDIT_ACADEMIC_SPACE), listOf(Groups.SYSTEM_ADMINISTRATOR, Groups.PROGRAM_DIRECTOR))
        val request = MockMvcRequestBuilders
            .put(Routes.ACADEMIC_SPACES_PATH + "/3")
            .content(objectMapper.writeValueAsString(academicSpace))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)

        val academicSpaces = academicSpaceRepository.findAll().toList()

        val academicSpaceToAssert = academicSpaces.find { it.id == academicSpace.id }

        Assert.assertNotNull(academicSpaceToAssert)
        Assert.assertEquals(academicSpace.academicProgram.id, academicSpaceToAssert?.academicProgram?.id)
        Assert.assertEquals(academicSpace.id, academicSpaceToAssert?.id)
    }

    @Test
    @Sql("/queries/AcademicSpaceControllerTest/editAcademicSpacesProgramNotFound.sql")
    fun editAcademicSpacesProgramNotFoundTest() {
        val faculty = Faculty(5, "Ingenieria", true)
        val academicProgram = AcademicProgram(10, "nameee", "sniees", 10, faculty, 5, true)
        val academicSpace = AcademicSpace(3, "name1", 8, 10, "modality2", academicProgram, true, 4)
        val header = mockSecurity("Token", listOf(Permissions.EDIT_ACADEMIC_SPACE), listOf(Groups.SYSTEM_ADMINISTRATOR, Groups.PROGRAM_DIRECTOR))
        val request = MockMvcRequestBuilders
            .put(Routes.ACADEMIC_SPACES_PATH + "/3")
            .content(objectMapper.writeValueAsString(academicSpace))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("The Program does not exist!")))
    }

    @Test
    @Sql("/queries/AcademicSpaceControllerTest/editAcademicSpacesNameInUse.sql")
    fun editAcademicSpacesNameInUseTest() {
        val academicProgram = academicProgramRepository.findById(1).orElse(null)
        val academicSpace = AcademicSpace(3, "name2", 8, 10, "modality2", academicProgram, true, 5)
        val header = mockSecurity("Token", listOf(Permissions.EDIT_ACADEMIC_SPACE), listOf(Groups.SYSTEM_ADMINISTRATOR, Groups.PROGRAM_DIRECTOR))
        val request = MockMvcRequestBuilders
            .put(Routes.ACADEMIC_SPACES_PATH + "/3")
            .content(objectMapper.writeValueAsString(academicSpace))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.value)
        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Name in use")))
    }

    @Test
    @Sql("/queries/AcademicSpaceControllerTest/getAllAcademicSpacesFromPrograms.sql")
    fun getAllAcademicSpacesFromProgramsTest() {
        val idProgram: Long = 1L

        val header = mockSecurity("Token", listOf(), listOf())
        val request = MockMvcRequestBuilders.get(
            "${Routes.ACADEMIC_PROGRAMS_PATH}/${Routes.GET_ALL_ACADEMIC_SPACES_BY_PROGRAM}",
            idProgram
        ).contentType(MediaType.APPLICATION_JSON_VALUE).header(header.name, header.value)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("content", Matchers.hasSize<Int>(4)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[0].academic_program.id", Matchers.`is`(1)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[1].academic_program.id", Matchers.`is`(1)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[2].academic_program.id", Matchers.`is`(1)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[3].academic_program.id", Matchers.`is`(1)))
            .andExpect(MockMvcResultMatchers.jsonPath("pageable.page_number", Matchers.`is`(0)))
            .andExpect(MockMvcResultMatchers.jsonPath("size", Matchers.`is`(4)))
            .andExpect(MockMvcResultMatchers.jsonPath("sort.sorted", Matchers.`is`(false)))
    }

    @Test
    @Sql("/queries/AcademicSpaceControllerTest/getAllAcademicSpacesFromProgramProgramDoesntExist.sql")
    fun getAllAcademicSpacesFromProgramProgramDoesntExistTest() {
        val idProgram: Long = 3L

        val header = mockSecurity("Token", listOf(), listOf())
        val request = MockMvcRequestBuilders.get(
            "${Routes.ACADEMIC_PROGRAMS_PATH}/${Routes.GET_ALL_ACADEMIC_SPACES_BY_PROGRAM}",
            idProgram
        ).contentType(MediaType.APPLICATION_JSON_VALUE).header(header.name, header.value)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("The Program does not exist!")))
    }

    @Test
    @Sql("/queries/AcademicSpaceControllerTest/getAcademicSpaceListBySemester.sql")
    fun getAcademicSpaceListBySemesterTest() {
        val idProgram: Long = 1L
        val semester = 3

        val header = mockSecurity("Token", listOf(), listOf())
        val request = MockMvcRequestBuilders.get(
            "${Routes.ACADEMIC_PROGRAMS_PATH}/${Routes.GET_ALL_ACADEMIC_SPACES_BY_SEMESTER}",
            idProgram,
            semester
        ).contentType(MediaType.APPLICATION_JSON_VALUE).header(header.name, header.value)
        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("content", Matchers.hasSize<Int>(2)))
    }

    @Test
    @Sql("/queries/AcademicSpaceControllerTest/getAcademicSpaceListBySemesterProgramDoesntExist.sql")
    fun getAcademicSpaceListBySemesterProgramDoesntExistTest() {
        val idProgram: Long = 100L
        val semester = null

        val header = mockSecurity("Token", listOf(), listOf())
        val request = MockMvcRequestBuilders.get(
            "${Routes.ACADEMIC_PROGRAMS_PATH}/${Routes.GET_ALL_ACADEMIC_SPACES_BY_SEMESTER}",
            idProgram,
            semester
        ).contentType(MediaType.APPLICATION_JSON_VALUE).header(header.name, header.value)
        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound).andExpect(
            MockMvcResultMatchers.jsonPath(
                "$.message",
                Matchers.`is`("The Program does not exist!")
            )
        )
    }

    @Test
    @Sql("/queries/AcademicSpaceControllerTest/activateAcademicSpace.sql")
    fun activateAcademicSpaceTest() {
        val header = mockSecurity("Token", listOf(Permissions.ACTIVATE_ACADEMIC_SPACE), listOf(Groups.SYSTEM_ADMINISTRATOR, Groups.PROGRAM_DIRECTOR))
        val request = MockMvcRequestBuilders.patch(
            Routes.ACADEMIC_SPACES_PATH + "/3/activate"
        ).header(header.name, header.value)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)

        val academicSpaces = academicSpaceRepository.findAll().toList()

        val academicSpaceToAssert = academicSpaces.find { it.id == 3L }
        Assert.assertNotNull(academicSpaceToAssert)
        Assert.assertEquals(true, academicSpaceToAssert?.enabled)
    }

    @Test
    fun activateAcademicSpaceNotExistTest() {
        val header = mockSecurity("Token", listOf(Permissions.ACTIVATE_ACADEMIC_SPACE), listOf(Groups.SYSTEM_ADMINISTRATOR, Groups.PROGRAM_DIRECTOR))
        val request = MockMvcRequestBuilders.patch(
            Routes.ACADEMIC_SPACES_PATH + "/15/activate"
        ).header(header.name, header.value)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Academic Space doesn´t exist.")))
    }

    @Test
    @Sql("/queries/AcademicSpaceControllerTest/activateAcademicSpaceAlreadyActivated.sql")
    fun activateAcademicSpaceAlreadyActivatedTest() {
        val header = mockSecurity("Token", listOf(Permissions.ACTIVATE_ACADEMIC_SPACE), listOf(Groups.SYSTEM_ADMINISTRATOR, Groups.PROGRAM_DIRECTOR))
        val request = MockMvcRequestBuilders.patch(
            Routes.ACADEMIC_SPACES_PATH + "/3/activate"
        ).header(header.name, header.value)
        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Academic Space already activated.")))
    }

    @Test
    @Sql("/queries/AcademicSpaceControllerTest/deactivateAcademicSpace.sql")
    fun deactivateAcademicSpaceTest() {
        val header = mockSecurity("Token", listOf(Permissions.DESACTIVE_ACADEMIC_SPACE), listOf(Groups.SYSTEM_ADMINISTRATOR, Groups.PROGRAM_DIRECTOR))
        val request = MockMvcRequestBuilders.patch(
            Routes.ACADEMIC_SPACES_PATH + "/3/deactivate"
        ).header(header.name, header.value)
        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)

        val academicSpaces = academicSpaceRepository.findAll().toList()

        val academicSpaceToAssert = academicSpaces.find { it.id == 3L }
        Assert.assertNotNull(academicSpaceToAssert)
        Assert.assertEquals(false, academicSpaceToAssert?.enabled)
    }

    @Test
    fun deactivateAcademicSpaceNotExistTest() {
        val header = mockSecurity("Token", listOf(Permissions.ACTIVATE_ACADEMIC_SPACE), listOf(Groups.SYSTEM_ADMINISTRATOR, Groups.PROGRAM_DIRECTOR))
        val request = MockMvcRequestBuilders.patch(
            Routes.ACADEMIC_SPACES_PATH + "/15/deactivate"
        ).header(header.name, header.value)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Academic Space doesn´t exist.")))
    }

    @Test
    @Sql("/queries/AcademicSpaceControllerTest/deactivateAcademicSpaceAlreadyActivated.sql")
    fun deactivateAcademicSpaceAlreadyActivatedTest() {
        val header = mockSecurity("Token", listOf(Permissions.ACTIVATE_ACADEMIC_SPACE), listOf(Groups.SYSTEM_ADMINISTRATOR, Groups.PROGRAM_DIRECTOR))
        val request = MockMvcRequestBuilders.patch(
            Routes.ACADEMIC_SPACES_PATH + "/3/deactivate"
        ).header(header.name, header.value)
        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(
                MockMvcResultMatchers.jsonPath(
                    "$.message",
                    Matchers.`is`("Academic Space already deactivated.")
                )
            )
    }

    @Test
    @Sql("/queries/AcademicSpaceControllerTest/find_academic_spaces_by_ids_test.sql")
    fun findAcademicSpacesByIdsTest() {

        val jsonBody =
            """{
                "ids":[100, 101, 102, 103]
            }"""

        val header = mockSecurity("Token", listOf(Permissions.FIND_ACADEMIC_SPACE), listOf(Groups.SYSTEM_ADMINISTRATOR, Groups.PROGRAM_DIRECTOR))
        val request =
            MockMvcRequestBuilders.post("${Routes.ACADEMIC_SPACES_PATH}${Routes.FIND_ACADEMIC_SPACES_BY_IDS_PATH}")
                .content(jsonBody)
                .contentType(MediaType.APPLICATION_JSON)
                .header(header.name, header.value)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize<Int>(4)))
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", Matchers.`is`("Space 1")))
            .andExpect(MockMvcResultMatchers.jsonPath("$[1].name", Matchers.`is`("Space 2")))
            .andExpect(MockMvcResultMatchers.jsonPath("$[2].name", Matchers.`is`("Space 3")))
            .andExpect(MockMvcResultMatchers.jsonPath("$[3].name", Matchers.`is`("Space 4")))
    }
}
