CREATE TABLE public.academic_periods (
	id varchar(255) NOT NULL,
	end_date timestamp NULL,
	start_date timestamp NULL,
	CONSTRAINT academic_periods_pkey PRIMARY KEY (id)
);

CREATE TABLE public.activities (
	id bigserial NOT NULL,
	description varchar(255) NULL,
	"name" varchar(255) NULL,
	CONSTRAINT activities_pkey PRIMARY KEY (id)
);

CREATE TABLE public.contract_loads (
	id bigserial NOT NULL,
	"name" varchar(255) NULL,
	weekly_hours_qty int4 NULL,
	enabled bool NOT NULL,
	CONSTRAINT contract_loads_pkey PRIMARY KEY (id)
);

CREATE TABLE public.faculties (
	id bigserial NOT NULL,
	"name" varchar(255) NULL,
	status bool NOT NULL,
	CONSTRAINT faculties_pkey PRIMARY KEY (id)
);

CREATE TABLE public.academic_programs (
	id bigserial NOT NULL,
	credits int4 NOT NULL,
	enabled bool NULL,
	"name" varchar(255) NULL,
	semester_qty int4 NULL,
	snies varchar(255) NULL,
	id_faculty int8 NULL,
	CONSTRAINT academic_programs_pkey PRIMARY KEY (id),
	CONSTRAINT fkdy1cb69dx2y4tm0khoy313rcd FOREIGN KEY (id_faculty) REFERENCES faculties(id)
);

CREATE TABLE public.academic_spaces (
	id bigserial NOT NULL,
	credits int4 NOT NULL,
	enabled bool NULL,
	max_faults int4 NULL,
	modality varchar(255) NULL,
	"name" varchar(255) NULL,
	id_academic_program int8 NULL,
	semester int4 NULL,
	CONSTRAINT academic_spaces_pkey PRIMARY KEY (id),
	CONSTRAINT fkt1swvrl6hdaru86uoyvp7fhq1 FOREIGN KEY (id_academic_program) REFERENCES academic_programs(id)
);