package edu.eam.ingesoft.academic.repositories

import edu.eam.ingesoft.academic.model.entities.AcademicSpace
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

@Repository
interface AcademicSpaceRepository : PagingAndSortingRepository<AcademicSpace, Long> {

    fun findByName(name: String): List<AcademicSpace>

    @Query("SELECT a FROM AcademicSpace a WHERE a.academicProgram.id = :idProgram")
    fun findAllByProgramId(idProgram: Long, pageable: Pageable?): Page<AcademicSpace>

    @Query("SELECT a FROM AcademicSpace a WHERE a.academicProgram.id = :idProgram AND a.semester = :semester")
    fun findAllBySemester(idProgram: Long, semester: Int, pageable: Pageable?): Page<AcademicSpace>
}
