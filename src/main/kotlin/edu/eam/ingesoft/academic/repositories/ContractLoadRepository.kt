package edu.eam.ingesoft.academic.repositories

import edu.eam.ingesoft.academic.model.entities.ContractLoad
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface ContractLoadRepository : CrudRepository<ContractLoad, Long> {
    fun existsByName(name: String): Boolean

    fun findByName(name: String): List<ContractLoad>
}
