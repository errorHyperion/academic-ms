package edu.eam.ingesoft.academic.repositories

import edu.eam.ingesoft.academic.model.entities.AcademicProgram
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

@Repository
interface AcademicProgramRepository : PagingAndSortingRepository<AcademicProgram, Long> {

    fun existsByName(name: String): Boolean

    @Query("SELECT prom FROM AcademicProgram prom WHERE prom.faculty.id = :idFaculty")
    fun findAllByFacultyId(idFaculty: Long, pageable: Pageable?): Page<AcademicProgram>
}
