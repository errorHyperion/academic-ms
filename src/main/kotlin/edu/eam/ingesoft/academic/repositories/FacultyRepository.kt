package edu.eam.ingesoft.academic.repositories

import edu.eam.ingesoft.academic.model.entities.Faculty
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface FacultyRepository : CrudRepository<Faculty, Long>
