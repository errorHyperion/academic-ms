package edu.eam.ingesoft.academic.services

import edu.eam.ingesoft.academic.exceptions.BusinessException
import edu.eam.ingesoft.academic.model.entities.Faculty
import edu.eam.ingesoft.academic.repositories.FacultyRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

@Service
class FacultyService {
    @Autowired
    lateinit var facultyRepository: FacultyRepository

    fun create(id: Long, name: String, status: Boolean) {

        val facultyToFind = facultyRepository.findById(id)

        if (!facultyToFind.isEmpty) throw BusinessException(
            "The faculty is already created",
            HttpStatus.PRECONDITION_FAILED
        )

        val facultyToCreate = Faculty(id, name, status)

        facultyRepository.save(facultyToCreate)
    }

    fun find(id: Long): Faculty {

        return facultyRepository.findById(id).orElse(null)

            ?: throw BusinessException("faculty Not Found", HttpStatus.NOT_FOUND)
    }

    fun editFaculty(id: Long, name: String, status: Boolean) {

        val facultyToFind = facultyRepository.findById(id)

        if (!facultyToFind.isPresent) throw BusinessException("Faculty Not Found", HttpStatus.NOT_FOUND)

        val facultyToSave = Faculty(id, name, status)

        facultyRepository.save(facultyToSave)
    }

    fun activateFaculty(id: Long) {

        val facultyToFind = facultyRepository.findById(id).orElse(null)

            ?: throw BusinessException("Faculty doesn't exist.", HttpStatus.NOT_FOUND)

        if (facultyToFind.status == true) throw BusinessException(
            "Faculty already activated.",
            HttpStatus.PRECONDITION_FAILED
        )

        facultyRepository.save(facultyToFind)
    }

    fun disableFaculty(id: Long) {

        val facultyToFind = facultyRepository.findById(id).orElse(null)

            ?: throw BusinessException("Faculty doesn't exist.", HttpStatus.NOT_FOUND)

        if (facultyToFind.status == false) throw BusinessException(

            "Faculty already deactivate.",

            HttpStatus.PRECONDITION_FAILED

        )

        facultyToFind.status = false

        facultyRepository.save(facultyToFind)
    }
}
