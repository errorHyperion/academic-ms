package edu.eam.ingesoft.academic.model.responses

data class ErrorResponse(
    val status: Int? = 500,
    val message: String? = "Exception"
)
