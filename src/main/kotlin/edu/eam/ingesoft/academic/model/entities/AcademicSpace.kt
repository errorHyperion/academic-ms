package edu.eam.ingesoft.academic.model.entities

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "academic_spaces")
data class AcademicSpace(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long,

    val name: String,

    val credits: Int,

    @Column(name = "max_faults")
    val maxFaults: Int,

    val modality: String,

    @ManyToOne
    @JoinColumn(name = "id_academic_program", referencedColumnName = "id")
    val academicProgram: AcademicProgram,

    var enabled: Boolean? = true,

    val semester: Int
)
