package edu.eam.ingesoft.academic.model.entities

import java.util.Date
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "academic_periods")
data class AcademicPeriod(
    @Id
    val id: String,

    @Column(name = "start_date")
    val startDate: Date,

    @Column(name = "end_date")
    val endDate: Date
)
