package edu.eam.ingesoft.academic.model.request

data class CreateFacultyRequest(
    val id: Long,
    val name: String,
    val status: Boolean
)
