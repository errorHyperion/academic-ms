package edu.eam.ingesoft.academic.model.request

data class DisableFacultyRequest(
    val id: Long,
    val name: String,
    val status: Boolean
)
