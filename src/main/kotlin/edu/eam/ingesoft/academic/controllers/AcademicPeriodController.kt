package edu.eam.ingesoft.academic.controllers

import edu.eam.ingesoft.academic.config.Permissions
import edu.eam.ingesoft.academic.config.Routes
import edu.eam.ingesoft.academic.security.Secured
import edu.eam.ingesoft.academic.services.AcademicPeriodService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(Routes.ACADEMICPERIOD_PATH)
class AcademicPeriodController {

    @Autowired
    lateinit var academicPeriodService: AcademicPeriodService

    @Secured(permissions = [Permissions.CHECK_CURRENT_ACADEMIC_PERIOD])
    @GetMapping(Routes.CURRENT_PERIOD_PTH)
    fun getCurrentAcademicPeriod() = academicPeriodService.findCurrentAcademicPeriod()
}
