package edu.eam.ingesoft.academic.controllers

import edu.eam.ingesoft.academic.config.Groups
import edu.eam.ingesoft.academic.config.Permissions
import edu.eam.ingesoft.academic.config.Routes
import edu.eam.ingesoft.academic.model.request.CreateFacultyRequest
import edu.eam.ingesoft.academic.model.request.EditFacultyRequest
import edu.eam.ingesoft.academic.security.Secured
import edu.eam.ingesoft.academic.services.FacultyService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(Routes.FACULTY_PATH)
class FacultyController {

    @Autowired
    lateinit var facultyService: FacultyService

    @Secured(permissions = [Permissions.CREATE_FACULTY], groups = [Groups.SYSTEM_ADMINISTRATOR])
    @PostMapping
    fun create(@RequestBody request: CreateFacultyRequest) =
        facultyService.create(request.id, request.name, request.status)

    @Secured(
        permissions = [Permissions.FIND_FACULTY],
        groups = [Groups.PROGRAM_DIRECTOR, Groups.SYSTEM_ADMINISTRATOR]
    )
    @GetMapping(Routes.SEARCH_FACULTY_PATH)
    fun find(@PathVariable id: Long) = facultyService.find(id)

    @Secured(permissions = [Permissions.EDIT_FACULTY], groups = [Groups.SYSTEM_ADMINISTRATOR])
    @PutMapping(Routes.EDIT_FACULTY_PATH)
    fun editFaculty(@PathVariable id: Long, @RequestBody request: EditFacultyRequest) =
        facultyService.editFaculty(id, request.name, request.status)

    @Secured(permissions = [Permissions.ACTIVATE_FACULTY], groups = [Groups.SYSTEM_ADMINISTRATOR])
    @PatchMapping(Routes.ACTIVATE_FACULTY_PATH)
    fun ActivateFaculty(@PathVariable("id") id: Long) = facultyService.activateFaculty(id)

    @Secured(permissions = [Permissions.DEACTIVATE_FACULTY], groups = [Groups.SYSTEM_ADMINISTRATOR])
    @PatchMapping(Routes.DISABLE_FACULTY_PATH)
    fun disableFaculty(@PathVariable("id") id: Long) = facultyService.disableFaculty(id)
}
