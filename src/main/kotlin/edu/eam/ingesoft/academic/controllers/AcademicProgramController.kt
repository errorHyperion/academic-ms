package edu.eam.ingesoft.academic.controllers

import edu.eam.ingesoft.academic.config.Groups
import edu.eam.ingesoft.academic.config.Permissions
import edu.eam.ingesoft.academic.config.Routes
import edu.eam.ingesoft.academic.model.entities.AcademicProgram
import edu.eam.ingesoft.academic.security.Secured
import edu.eam.ingesoft.academic.services.AcademicProgramService
import edu.eam.ingesoft.academic.services.AcademicSpaceService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PageableDefault
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(Routes.ACADEMIC_PROGRAMS_PATH)

class AcademicProgramController {

    @Autowired
    lateinit var academicProgramService: AcademicProgramService

    @Autowired
    lateinit var academicSpaceService: AcademicSpaceService

    @Secured(
        permissions = [Permissions.CREATE_PROGRAM],
        groups = [Groups.PROGRAM_DIRECTOR, Groups.SYSTEM_ADMINISTRATOR]
    )
    @PostMapping
    fun create(@RequestBody academicProgram: AcademicProgram) {
        academicProgramService.create(academicProgram)
    }

    @Secured(permissions = [Permissions.FIND_ACADEMIC_PROGRAM], groups = [Groups.SYSTEM_ADMINISTRATOR])
    @GetMapping(Routes.FIND_PROGRAM_PATH)
    fun find(@PathVariable idProgram: Long) = academicProgramService.find(idProgram)

    @Secured(permissions = [Permissions.LIST_PROGRAMS_BY_FACULTY], groups = [Groups.SYSTEM_ADMINISTRATOR])
    @GetMapping(Routes.GET_ALL_PROGRAMS_BY_FACULTY_ID)
    fun getAllProgramsFromFaculty(@PathVariable idFaculty: Long, pageable: Pageable) =
        academicProgramService.getProgramList(idFaculty, pageable)

    @Secured(permissions = [Permissions.ACTIVE_PROGRAM], groups = [Groups.SYSTEM_ADMINISTRATOR])
    @PatchMapping(Routes.CHANGE_STATUS_PROGRAM_ACTIVE)
    fun activeProgram(@PathVariable idProgram: Long) = academicProgramService.activate(idProgram)

    @Secured(
        permissions = [Permissions.DESACTIVE_PROGRAM],
        groups = [Groups.SYSTEM_ADMINISTRATOR]
    )
    @PatchMapping(Routes.CHANGE_STATUS_PROGRAM_DESACTIVE)
    fun deactivateProgram(@PathVariable idProgram: Long) = academicProgramService.desactive(idProgram)

    @Secured(permissions = [Permissions.EDIT_ACADEMIC_PROGRAM], groups = [Groups.SYSTEM_ADMINISTRATOR])
    @PutMapping(Routes.EDIT_PROGRAM)
    fun editProgram(@RequestBody academicProgram: AcademicProgram, @PathVariable("idProgram") idProgram: Long) =
        academicProgramService.editProgram(academicProgram, idProgram)

    @Secured
    @GetMapping(Routes.GET_ALL_ACADEMIC_SPACES_BY_PROGRAM)
    fun getAllAcademicSpacesFromSemester(
        @PathVariable idProgram: Long,
        @RequestParam semester: Int?,
        @PageableDefault(size = 4) pageable: Pageable
    ) =
        academicSpaceService.getAcademicSpaceListBySemester(idProgram, semester, pageable)
}
