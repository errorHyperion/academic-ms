package edu.eam.ingesoft.academic.controllers

import edu.eam.ingesoft.academic.config.Groups
import edu.eam.ingesoft.academic.config.Permissions
import edu.eam.ingesoft.academic.config.Routes
import edu.eam.ingesoft.academic.model.entities.ContractLoad
import edu.eam.ingesoft.academic.security.Secured
import edu.eam.ingesoft.academic.services.ContractLoadServices
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(Routes.CONTRACT_PATH)
class ContractLoadController {

    @Autowired
    lateinit var contractLoadService: ContractLoadServices

    @Secured(permissions = [Permissions.CREATE_CONTRACT_LOAD], groups = [Groups.SYSTEM_ADMINISTRATOR])
    @PostMapping
    fun create(@RequestBody contractLoad: ContractLoad) = contractLoadService.create(contractLoad)

    @Secured(permissions = [Permissions.EDIT_CONTRACT_TYPE], groups = [Groups.SYSTEM_ADMINISTRATOR])
    @PutMapping(Routes.EDIT_CONTRACT_PATH)
    fun edit(@RequestBody contractLoad: ContractLoad, @PathVariable("id") id: Long) =
        contractLoadService.edit(contractLoad, id)

    @GetMapping()
    fun finContractLoad() = contractLoadService.findContractLoad()

    @Secured(permissions = [Permissions.DEACTIVATE_CONTRACT_TYPE], groups = [Groups.SYSTEM_ADMINISTRATOR])
    @PatchMapping(Routes.DEACTIVATE_CONTRACT_PATH)
    fun deactivate(@PathVariable("id") id: Long) = contractLoadService.deactivate(id)

    @Secured(permissions = [Permissions.ACTIVATE_CONTRACT_TYPE], groups = [Groups.SYSTEM_ADMINISTRATOR])
    @PatchMapping(Routes.ACTIVATE_CONTRACT_PTH)
    fun activate(@PathVariable("id") id: Long) = contractLoadService.activate(id)
}
