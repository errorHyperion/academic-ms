package edu.eam.ingesoft.academic.security.authclient.model

data class Header(
    val name: String,
    val value: String
)
