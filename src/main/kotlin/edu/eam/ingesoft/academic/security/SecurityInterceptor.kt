package edu.eam.ingesoft.academic.security

import edu.eam.ingesoft.academic.security.authclient.SecurityService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.web.method.HandlerMethod
import org.springframework.web.servlet.HandlerInterceptor
import org.springframework.web.servlet.ModelAndView
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class SecurityInterceptor : HandlerInterceptor {

    @Autowired
    lateinit var securityService: SecurityService

    @Throws(Exception::class)
    fun getToken(request: HttpServletRequest): String {
        val bearer = request.getHeader("Authorization") ?: throw SecurityException("Token required")

        if (!bearer.startsWith(prefix = "Bearer")) {
            throw SecurityException("Invalid token")
        }
        val token = bearer.substring(7)
        return token
    }

    @Throws(Exception::class)
    override fun preHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any): Boolean {
        if (handler is HandlerMethod) {
            val securityFilter: Secured = handler.method.getAnnotation(Secured::class.java) ?: return true

            if (securityFilter != null) {
                val token = getToken(request)

                val securityPayload = securityService.validateToken(token)
                val userPermissions = securityPayload.permissions
                val userGroups = securityPayload.groups

                val handlerPermissions = securityFilter.permissions
                val handlerGroups = securityFilter.groups

                if (handlerPermissions.isEmpty() and handlerGroups.isEmpty()) return true

                if (handlerPermissions.intersect(userPermissions).isEmpty() and handlerGroups.intersect(userGroups).isEmpty()
                ) {
                    throw SecurityException("User has not access to this function.")
                }
            }
        }
        return true
    }

    @Throws(Exception::class)
    override fun postHandle(
        request: HttpServletRequest,
        response: HttpServletResponse,
        handler: Any,
        modelAndView: ModelAndView?
    ) {
        super.postHandle(request, response, handler, modelAndView)
    }

    @Throws(Exception::class)
    override fun afterCompletion(
        request: HttpServletRequest,
        response: HttpServletResponse,
        handler: Any,
        exception: java.lang.Exception?
    ) {
        println("After completion of request and response")
    }
}
