package edu.eam.ingesoft.academic.security

class SecurityException(message: String?) : RuntimeException(message)
