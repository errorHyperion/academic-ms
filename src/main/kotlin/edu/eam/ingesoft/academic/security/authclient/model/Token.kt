package edu.eam.ingesoft.academic.security.authclient.model

data class Token(
    val token: String
)
